from Dimension import *


class ThreeD(Dimension):
    def __init__(self, master):
        # Create main frame
        frame = Frame(master, cursor="ul_angle")
        frame.pack()
        # Calls the functions to be used
        self.makeInputs(frame)

    def wavefunction_3d(self, p, q, r):
        x, y, z = np.ogrid[0:p:64j, 0:q:64j, 0:r:64j]
        scalars = abs((np.sin((x * np.pi))) * (np.sin((y * np.pi))) * (np.sin((z * np.pi)))) ** 2
        obj = contour3d(scalars)
        return axes().obj

    def makeInputs(self, frame):
        # builds the frame which will hold all the inputs and their labels
        InputFrame = Frame(frame)
        # places this frame at co-ord (0,0) of the master frame
        InputFrame.grid(column=0, row=0)

        # LABELS FOR QUANTUM NUMBERS
        self.lblp = Label(InputFrame, text="Quantum Number p", justify=LEFT)  # p Label
        self.lblp.grid(column=0, row=0)
        self.lblq = Label(InputFrame, text="Quantum Number q", justify=LEFT)  # q Label
        self.lblq.grid(column=0, row=1)
        self.lblr = Label(InputFrame, text="Quantum Number r", justify=LEFT)  # r Label
        self.lblr.grid(column=0, row=2)

        # ENTRY BOXES FOR QUANTUM NUMBERS
        self.entp = Entry(InputFrame)  # p Entry Box
        self.entp.grid(column=1, row=0)
        self.entq = Entry(InputFrame)  # q Entry Box
        self.entq.grid(column=1, row=1)
        self.entr = Entry(InputFrame)  # r Entry Box
        self.entr.grid(column=1, row=2)

        # Plot Wavefunction Button
        self.butPlot = Button(InputFrame, text='PLOT', command=self.calcPatternWave)
        self.butPlot.grid(column=0, row=9)

        # Clear Button
        self.c = Button(InputFrame, text='CLEAR', command=self.clear)
        self.c.grid(column=1, row=9)

    def clear(self):
        clf()

    def calcPatternWave(self):
        p = int(self.entp.get())  # .get returns a string from that object
        q = int(self.entq.get())
        r = int(self.entr.get())
        print self.wavefunction_3d(p, q, r)
