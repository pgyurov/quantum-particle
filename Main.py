from Menu import *

m = Menu
root = Tk()
GUI = m(root)
root.wm_title("Particle In A Box")
root.maxsize(998, 448)  # stops the user from expanding window size (to retain aesthetic properties)
root.minsize(998, 448)
root.mainloop()  # run the main loop
